--- beamer-subtitle - transform heading with subtitle class as beamer subtitle
---
--- idea from https://github.com/jgm/pandoc/issues/5031#issuecomment-1382166649

if FORMAT:match 'beamer' then
  function Header (elem)
     if elem.level == (PANDOC_WRITER_OPTIONS.slide_level + 1) and
        elem.classes:includes("subtitle", 1) then
        return {
           pandoc.RawInline('latex',
                            '\\framesubtitle{'
                            .. pandoc.utils.stringify(elem.content)
                            .. '}')
        }
     else
        return elem
     end
  end
end

---
title: A simple title
author: Someone
date: Today
theme: boadilla
---

# A section

## A slide

This one has no subtitle

### But it has a block

#### Which is really a block {.subtitle }

## Another slide

### This one has a subtitle {.subtitle}

It proves that it works

### It has also a block

Without any value.

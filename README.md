I store here some of [pandoc filters](https://pandoc.org/lua-filters.html) I oftenly use.

- [beamer-subtitle](beamer-subtitle) - transform heading with subtitle class as beamer subtitle
- [center.lua](center.lua) - center image on their line and div content
- [include-code-files.lua](include-code-files.lua) - include code from source files
- [include-files.lua](include-files.lua) - include Markdown files
- [svg-image-to-pdf.lua](svg-image-to-pdf.lua) - filter to replace svg filename by pdf ones
